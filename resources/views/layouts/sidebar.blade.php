<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">Menu</li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-airplay"></i>
                        <span> Dashboard </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('categories') }}">
                        <i class="fe-grid"></i>
                        <span> Category </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('forms/online-audit ') }}">
                        <i class="fe-file-text"></i>
                        <span> Online Audit </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('forms/validation-audit') }}">
                        <i class="fe-activity"></i>
                        <span> Validation Audit </span>
                    </a>
                </li>
                


                <li class="menu-title mt-2">Admin</li>

                <li>
                    <a href="{{ url('admin') }}">
                        <i class="fe-star-on"></i>
                        <span> Admin </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('admin/auditors') }}">
                        <i class="fe-edit-1"></i>
                        <span> Auditor </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('admin/members') }}">
                        <i class="fe-users"></i>
                        <span> Users </span>
                    </a>
                </li>

                <li class="menu-title mt-2">Setting</li>

                <li>
                    <a href="{{ url('profiles') }}">
                        <i class="fe-user"></i>
                        <span> My Account </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('profile/passworsd') }}">
                        <i class="fe-settings"></i>
                        <span> Change Paassword </span>
                    </a>
                </li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->