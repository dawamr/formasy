<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard.index');
});

Route::resource('categories', 'CategoryController');
Route::group(['prefix' => 'forms'], function () {
    Route::resource('online-audit', 'OnlineAuditFormController');
    Route::resource('validation-audit', 'ValidationAuditFormController');
});
Route::group(['prefix' => 'members'], function () {
    Route::resource('online-audit', 'OnlineAuditFormMemberController');
    Route::resource('validation-audit', 'ValidationAuditFormMemberController');
});
Route::group(['prefix' => 'questions'], function () {
    Route::resource('online-audit', 'OnlineAuditFormQuestionController');
    Route::resource('validation-audit', 'ValidationAuditFormQuestionController');
});
Route::group(['prefix' => 'responses'], function () {
    Route::resource('online-audit', 'OnlineAuditFormResponseController');
    Route::resource('validation-audit', 'ValidationAuditFormResponseController');
});
Route::group(['prefix' => 'admins'], function () {
    Route::resource('/', 'UserController');
    Route::resource('auditors', 'UserController');
    Route::resource('members', 'UserController');
});
Route::group(['prefix' => 'profiles'], function () {
    Route::get('/', function () {
        
    });
    Route::get('password', function () {
        
    });
});