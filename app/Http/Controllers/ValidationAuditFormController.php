<?php

namespace App\Http\Controllers;

use App\ValidationAuditForm;
use Illuminate\Http\Request;

class ValidationAuditFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ValidationAuditForm  $validationAuditForm
     * @return \Illuminate\Http\Response
     */
    public function show(ValidationAuditForm $validationAuditForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ValidationAuditForm  $validationAuditForm
     * @return \Illuminate\Http\Response
     */
    public function edit(ValidationAuditForm $validationAuditForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ValidationAuditForm  $validationAuditForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ValidationAuditForm $validationAuditForm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ValidationAuditForm  $validationAuditForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(ValidationAuditForm $validationAuditForm)
    {
        //
    }
}
