<?php

namespace App\Http\Controllers;

use App\OnlineAuditForm;
use Illuminate\Http\Request;

class OnlineAuditFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OnlineAuditForm  $onlineAuditForm
     * @return \Illuminate\Http\Response
     */
    public function show(OnlineAuditForm $onlineAuditForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OnlineAuditForm  $onlineAuditForm
     * @return \Illuminate\Http\Response
     */
    public function edit(OnlineAuditForm $onlineAuditForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OnlineAuditForm  $onlineAuditForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OnlineAuditForm $onlineAuditForm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OnlineAuditForm  $onlineAuditForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(OnlineAuditForm $onlineAuditForm)
    {
        //
    }
}
