<?php

namespace App\Http\Controllers;

use App\OnlineAuditFormMember;
use Illuminate\Http\Request;

class OnlineAuditFormMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OnlineAuditFormMember  $onlineAuditFormMember
     * @return \Illuminate\Http\Response
     */
    public function show(OnlineAuditFormMember $onlineAuditFormMember)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OnlineAuditFormMember  $onlineAuditFormMember
     * @return \Illuminate\Http\Response
     */
    public function edit(OnlineAuditFormMember $onlineAuditFormMember)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OnlineAuditFormMember  $onlineAuditFormMember
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OnlineAuditFormMember $onlineAuditFormMember)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OnlineAuditFormMember  $onlineAuditFormMember
     * @return \Illuminate\Http\Response
     */
    public function destroy(OnlineAuditFormMember $onlineAuditFormMember)
    {
        //
    }
}
