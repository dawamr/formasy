<?php

namespace App\Http\Controllers;

use App\ValidationAuditFormMember;
use Illuminate\Http\Request;

class ValidationAuditFormMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ValidationAuditFormMember  $validationAuditFormMember
     * @return \Illuminate\Http\Response
     */
    public function show(ValidationAuditFormMember $validationAuditFormMember)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ValidationAuditFormMember  $validationAuditFormMember
     * @return \Illuminate\Http\Response
     */
    public function edit(ValidationAuditFormMember $validationAuditFormMember)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ValidationAuditFormMember  $validationAuditFormMember
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ValidationAuditFormMember $validationAuditFormMember)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ValidationAuditFormMember  $validationAuditFormMember
     * @return \Illuminate\Http\Response
     */
    public function destroy(ValidationAuditFormMember $validationAuditFormMember)
    {
        //
    }
}
