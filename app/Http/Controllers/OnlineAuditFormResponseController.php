<?php

namespace App\Http\Controllers;

use App\OnlineAuditFormResponse;
use Illuminate\Http\Request;

class OnlineAuditFormResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OnlineAuditFormResponse  $onlineAuditFormResponse
     * @return \Illuminate\Http\Response
     */
    public function show(OnlineAuditFormResponse $onlineAuditFormResponse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OnlineAuditFormResponse  $onlineAuditFormResponse
     * @return \Illuminate\Http\Response
     */
    public function edit(OnlineAuditFormResponse $onlineAuditFormResponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OnlineAuditFormResponse  $onlineAuditFormResponse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OnlineAuditFormResponse $onlineAuditFormResponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OnlineAuditFormResponse  $onlineAuditFormResponse
     * @return \Illuminate\Http\Response
     */
    public function destroy(OnlineAuditFormResponse $onlineAuditFormResponse)
    {
        //
    }
}
