<?php

namespace App\Http\Controllers;

use App\OnlineAuditFormQuestion;
use Illuminate\Http\Request;

class OnlineAuditFormQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OnlineAuditFormQuestion  $onlineAuditFormQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(OnlineAuditFormQuestion $onlineAuditFormQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OnlineAuditFormQuestion  $onlineAuditFormQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(OnlineAuditFormQuestion $onlineAuditFormQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OnlineAuditFormQuestion  $onlineAuditFormQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OnlineAuditFormQuestion $onlineAuditFormQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OnlineAuditFormQuestion  $onlineAuditFormQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(OnlineAuditFormQuestion $onlineAuditFormQuestion)
    {
        //
    }
}
