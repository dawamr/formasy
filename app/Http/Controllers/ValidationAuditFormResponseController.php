<?php

namespace App\Http\Controllers;

use App\ValidationAuditFormResponse;
use Illuminate\Http\Request;

class ValidationAuditFormResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ValidationAuditFormResponse  $validationAuditFormResponse
     * @return \Illuminate\Http\Response
     */
    public function show(ValidationAuditFormResponse $validationAuditFormResponse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ValidationAuditFormResponse  $validationAuditFormResponse
     * @return \Illuminate\Http\Response
     */
    public function edit(ValidationAuditFormResponse $validationAuditFormResponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ValidationAuditFormResponse  $validationAuditFormResponse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ValidationAuditFormResponse $validationAuditFormResponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ValidationAuditFormResponse  $validationAuditFormResponse
     * @return \Illuminate\Http\Response
     */
    public function destroy(ValidationAuditFormResponse $validationAuditFormResponse)
    {
        //
    }
}
